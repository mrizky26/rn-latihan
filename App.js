import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Login from './src/container/login/login'
import Signup from './src/container/signup/signup'
import Tampilan1, { Screen1 } from './src/container/Tampilan/Screen1'
import Tampilan2, { Screen2 } from './src/container/Tampilan/Screen2'
import Tampilan3, { Screen3 } from './src/container/Tampilan/Screen3'
import Tampilan4, { Screen4 } from './src/container/Tampilan/Screen4'
import Tampilan5, { Screen5 } from './src/container/Tampilan/Screen5'
import Icon from 'react-native-vector-icons/AntDesign'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
const Stack = createStackNavigator()

export class App extends Component {
  render() {
    return (
      <Login/>
    )
  }
}

export default App