import React, { Component } from 'react'
import { View } from 'react-native'

class LayoutExercise extends Component {
    render() {
        return (
            <View style={{flex:1}}>

                <View style={{flex:1, flexDirection: 'row'}}>
                    <View style={{flex:1, backgroundColor: 'red'}} />
                    <View style={{flex:1, backgroundColor: 'green'}} />
                </View>

                <View style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
                    <View style={{width: '30%', backgroundColor: 'purple'}} />
                    <View style={{width: '30%', backgroundColor: 'purple'}} />
                </View>
                
                <View style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
                    <View style={{flex:1, backgroundColor: 'blue'}} />
                    <View style={{flex:1, backgroundColor: 'green'}} />
                    <View style={{flex:1}}>
                        <View style={{flex:1, backgroundColor: 'yellow'}} />
                        <View style={{flex:1, backgroundColor: 'red'}} />
                    </View>
                </View>
            
            </View>
        )
    }
}

export default LayoutExercise

                


