import React, { Component } from 'react'
import { View } from 'react-native'

class LayoutExercise extends Component {
    render() {
        return (
            <View style={{flex:1}}>

                {/* kolom 1 */}
                <View style={{flex:1, flexDirection: 'row'}}>
                    <View style={{flex:1, backgroundColor: '#3498DB'}} />
                    <View style={{flex:1, backgroundColor: '#E74C3C'}} />
                </View>

                {/* kolom  2 */}
                <View style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
                    <View style={{flex:1, backgroundColor: '#8E44AD'}} />
                    <View style={{width:'45%', backgroundColor: '#F1C40F'}} />
                    <View style={{flex:1}}>
                        <View style={{flex:1, backgroundColor: '#27AE60'}} />
                        <View style={{flex:1, backgroundColor: '#E67E22'}} />
                    </View>
                </View>
                
                {/* Kolom 3 */}
                <View style={{flex:0.2, backgroundColor:'#3498DB',justifyContent:'space-between'}}/>

                {/* Kolom 4 */}
                <View style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
                    <View style={{flex:1}}>
                        <View style={{flex:1, backgroundColor: '#8E44AD'}} />
                        <View style={{flex:1, backgroundColor: '#E74C3C'}} />
                    </View>
                    <View style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
                        <View style={{flex:1, backgroundColor: 'white'}} />
                        <View style={{width:'50%', backgroundColor: '#27AE60'}} />
                        <View style={{flex:1, backgroundColor: 'white'}} />
                    </View>
                    <View style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
                        <View style={{flex:1}}>
                            <View style={{flex:1, backgroundColor: '#E74C3C'}} />
                            <View style={{flex:1, backgroundColor: '#8E44AD'}} />
                        </View>
                    </View>
                </View>

                {/* kolom 5 */}
                <View style={{flex:1, padding:60, backgroundColor: '#F1C40F'}}>
                            <View style={{flex:1}}>
                            <View style={{flex:1, backgroundColor: '#E84393'}} />
                
                        
                </View>
                

                </View>

            </View>
        )
    }
}

export default LayoutExercise