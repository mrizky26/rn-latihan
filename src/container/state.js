import React, { Component }  from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native'

class StateExercise extends Component {
    constructor(){
        super()
        this.state = { 
            nama: 'Mochammad',
            inputan: ''
        }
    }

    buttonPress = () =>{
        this.setState({
            nama: ''
        })
    }

    render() {
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>

                <Text>{JSON.stringify(this.state)}</Text>

                <View style={{backgroundColor: 'white'}} >
                    <Text>{this.state.nama}</Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={()=>this.buttonPress()} >
                <TextInput onChangeText={(e)=>this.setState({inputan:e})} placeholder= 'masukan nama' />
                </TouchableOpacity>


                <TouchableOpacity style={styles.button} onPress={()=>this.buttonPress()} >
                    <Text >SUBMIT</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

export default StateExercise

const styles = StyleSheet.create({
    button: {
        width: 300,
        paddingVertical: 16,
        marginTop: 20,
        borderRadius: 8,
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 4
    }
    
})
