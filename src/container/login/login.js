import React, { Component } from 'react'
import { TextBase, View, StyleSheet, Image, TextInput, Button, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'

class App extends Component{
    render (){
        return(
            <View style={styles.container}>
                <Image source={require('../../assets/untar.png')} style={styles.logo} />
                <TextInput placeholder='Username / Email'style={styles.input} />
                <TextInput placeholder='Password' style={styles.input} />
                <TouchableOpacity style={styles.Button}>
                    <Text style={{color:'black'}}>LOGIN</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop: 20}}>
                    <Text style={{color : 'black'}}>Forgot Password ? </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop: 20}}>
                    <Text style={{color : 'blue'}}>Reset Password </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop: 20}}>
                    <Text style={{color : 'black'}}>Don't have an account ? </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop: 20}}>
                    <Text style={{color : 'blue'}}>Sign Up </Text>
                </TouchableOpacity>
            </View> 
        )   
    }    
}

export default App

const styles= StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    logo: {
        width: 200,
        height: 200,
    },
    input: {
        width: 300,
        borderRadius: 8,
        marginTop: 20,
        elevation: 2,
        backgroundColor: 'white' ,
        paddingLeft: 16
    },
    Button: {
        width: 300,
        paddingVertical: 16,
        marginTop: 20,
        borderRadius: 8,
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 4
    }
})
